FROM openjdk:8-jdk-alpine
ARG Option
ENV Option=$Option
ARG H2="h2"
ENV H2=$H2
COPY /target/*.jar /app/assignment.jar
WORKDIR /app
USER nobody
EXPOSE 8090
ENTRYPOINT java ${Option} -jar -Dspring.profiles.active=${H2} assignment.jar